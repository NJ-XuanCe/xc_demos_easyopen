

* 商品模块

  * [添加商品](api/goods.add.md)
  * [获取商品](api/goods.get.md)
  * [获取商品列表](api/goods.list2.0.md)
  * [获取商品列表](api/goods.pageinfo1.0.md)
  * [获取商品列表](api/goods.pageinfo2.0.md)
  * [获取用户商品(accessToken)](api/user.goods.get.md)
  * [获取用户商品(jwt)](api/userjwt.goods.get.md)


* 文档demo，参考DocDemoApi.java

  * [参数方式4,自定义属性，第一个](api/doc.param.4.md)
  * [参数方式5，外部类指定参数，可复用，第二个](api/doc.param.5.md)
  * [参数方式1,默认](api/doc.param.1.md)
  * [参数方式2,继承](api/doc.param.2.md)
  * [参数方式3,聚合](api/doc.param.3.md)
  * [参数方式6，数组参数](api/doc.param.6.md)
  * [参数方式7，数组对象参数](api/doc.param.7.md)
  * [参数方式8，重复参数](api/doc.param.8.md)
  * [参数方式9，嵌套参数](api/doc.param.9.md)
  * [返回结果0,没有返回结果](api/doc.result.0.md)
  * [返回结果1,默认](api/doc.result.1.md)
  * [返回结果10，多个list同样元素](api/doc.result.10.md)
  * [返回结果11，嵌套问题（菜单）](api/doc.result.11.md)
  * [返回结果12，嵌套对象](api/doc.result.12.md)
  * [返回结果2,指定返回结果类](api/doc.result.2.md)
  * [返回结果3,自定义字段](api/doc.result.3.md)
  * [返回结果4,返回List](api/doc.result.4.md)
  * [返回结果5,外部指定](api/doc.result.5.md)
  * [返回结果6,自定义类](api/doc.result.6.md)
  * [返回结果7,模板复用](api/doc.result.7.md)
  * [返回结果8,最外部包装类](api/doc.result.8.md)
  * [返回结果9](api/doc.result.9.md)


* 参数类型demo

  * [参数类型，自定义类](api/param.type.1.md)
  * [参数类型，JSONObject](api/param.type.2.md)
  * [参数类型，Map接收](api/param.type.3.md)
  * [参数类型，String接收](api/param.type.4.md)
  * [参数类型，int接收](api/param.type.5.md)
  * [参数类型，date](api/param.type.6.md)
  * [参数类型，枚举](api/param.type.7.md)
  * [参数类型，数组](api/param.type.8.md)
  * [参数类型，默认长度](api/param.type.9.md)


* 文件上传

  * [文件上传](api/file.upload.md)
  * [文件上传,不确定数量](api/file.upload3.md)
